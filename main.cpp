/*
    TO COMPILE: 
    g++ -std=c++17 main.cpp -o project.wt -lsqlite3 -I/usr/local/include -L/usr/local/lib -I/usr/local -L/usr/local -lwthttp -lwt -lboost_system -lboost_thread -lboost_filesystem -lboost_program_options -lsqlite3 -lwtdbo -lwtdbosqlite3

    TO EXECUTE:
    ./project.wt --docroot . --http-address 0.0.0.0 --http-port 9090
    firefox 0.0.0.0:9090
    
    INSTALLATION:
    sudo apt-get install gcc g++ libboost-all-dev cmake sqlite3 libsqlite3-dev
    
    https://www.webtoolkit.eu/wt/download
    cd wt-x.y.z
    mkdir build; cd build
    cmake ..
    make -j8
    sudo make install
    sudo ldconfig
*/

#include <Wt/WApplication.h>
#include <Wt/WBreak.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WLineEdit.h>
#include <Wt/WPushButton.h>
#include <Wt/WText.h>
#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/backend/Sqlite3.h>
#include <sqlite3.h>
#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <map>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <algorithm>

#define DATABASE "database"
#define DATAFILE "data.txt"
#define HEADLINE "Sentences Forum!"
#define ADMIN_USER "admin"
#define ADMIN_PASS "admin"

namespace wt  = Wt;
namespace dbo = Wt::Dbo;
using DatabaseData_t = std::map<int, std::pair<std::string, std::string>>;

DatabaseData_t DatabaseData;
static int CallbackSql(void *NotUsed, int argc, char **argv, char **azColName)
{
    int id;
    std::string name, password;

    for(auto i = 0; i < argc; i++)
    {
        if(strcmp(azColName[i], "id") == 0)
            id = std::atoi(argv[i]);
        
        else if(strcmp(azColName[i], "name") == 0)
            name = std::string(argv[i]);
        
        else if(strcmp(azColName[i], "password") == 0)
            password = std::string(argv[i]);
    
        if(i % 3 == 0 && (i != 0))
            DatabaseData[id] = std::make_pair(name, password);
    }
    
    return 0;
}

namespace Helpers {
	inline void show() {};
	inline void hide() {};

	template<typename T, typename... Args> inline void show(T& t, Args... args)
	{
		t -> show();
		show(args...);
	}

	template<typename T, typename... Args> inline void hide(T& t, Args... args)
	{
		t -> hide();
		hide(args...);
	}
};

struct User
{
    std::string name;
    std::string password;

    template<class Action>
    void persist(Action& a)
    {
        dbo::field(a, name,     "name");
        dbo::field(a, password, "password");
    }
}; 

class MainApplication : public Wt::WApplication {
	private:
        wt::WText* txtTitle;
        std::vector<wt::WText*> allPosts;
        wt::WPushButton* btnHome;
        wt::WPushButton* btnLogin;
        wt::WPushButton* btnRegister;

        // Database
        void setupDatabase();
        dbo::Session session;

        // Home
        void isHome();
        wt::WText* txtLoggedName;
        wt::WText* txtCommitText;
        wt::WLineEdit* editCommitText;
        wt::WPushButton* btnCommitText;
        wt::WText* txtDeletePost;
        wt::WLineEdit* editDeletePost;
        wt::WPushButton* btnDeletePost;

       // Login
        void isLogin();
        wt::WText* txtLoginName;
        wt::WText* txtLoginPass;
        wt::WLineEdit* editLoginName;
        wt::WLineEdit* editLoginPass;
        wt::WPushButton* btnLoginSystem;
        wt::WText* LoginTxtMsg;

        // Register
        void isRegister();
        wt::WText* txtRegisterName;
        wt::WText* txtRegisterPass;
        wt::WLineEdit* editRegisterName;
        wt::WLineEdit* editRegisterPass;
        wt::WPushButton* btnRegisterSystem;
        wt::WText* RegisterTxtMsg;

        

	public:
		MainApplication(const wt::WEnvironment& env);

		~MainApplication() { allPosts.clear(); }
};

MainApplication::MainApplication(const wt::WEnvironment& env) : wt::WApplication(env)
{
	setupDatabase();

	setTitle(HEADLINE);

	txtTitle = root() -> addWidget(std::make_unique<wt::WText>("<h1 style='color:blue;'>" + std::string(HEADLINE) + "</h1>"));
    txtTitle -> setMargin(700, wt::Side::Left);
    
    btnHome = root() -> addWidget(std::make_unique<wt::WPushButton>("Home"));
    btnHome -> disable();
    
    btnLogin = root() -> addWidget(std::make_unique<wt::WPushButton>("Login"));
    
    btnRegister = root() -> addWidget(std::make_unique<wt::WPushButton>("Register"));

	btnHome -> clicked().connect(this, [this](){
        btnHome ->      disable();
        btnLogin ->     enable();
        btnRegister ->  enable();

        isHome();	
	});

    btnLogin -> clicked().connect(this, [this](){
        btnHome ->      enable();
        btnLogin ->     disable();
        btnRegister ->  enable();

        this -> isLogin();
    });

    btnRegister -> clicked().connect(this, [this](){
        btnHome ->      enable();
        btnLogin ->     enable();
        btnRegister ->  disable();

        this -> isRegister();
    });

    txtLoggedName = root() -> addWidget(std::make_unique<wt::WText>("<h3>Logged in: Visitor</h3>"));
    txtCommitText = root() -> addWidget(std::make_unique<wt::WText>("<h4>Commit Text:</h4>"));
    editCommitText = root() -> addWidget(std::make_unique<wt::WLineEdit>());
    btnCommitText = root() -> addWidget(std::make_unique<wt::WPushButton>("Commit Text"));
    { auto downLine = root() -> addWidget(std::make_unique<wt::WText>("<h1></h1>")); }
    txtDeletePost = root() -> addWidget(std::make_unique<wt::WText>("<h4>Enter the sentence to delete:</h4>"));
    editDeletePost = root() -> addWidget(std::make_unique<wt::WLineEdit>());
    btnDeletePost = root() -> addWidget(std::make_unique<wt::WPushButton>("Delete Post"));
    { auto downLine = root() -> addWidget(std::make_unique<wt::WText>("<h1></h1>")); }
	Helpers::hide(txtCommitText, editCommitText, btnCommitText, txtDeletePost, editDeletePost, btnDeletePost);

    txtLoginName = root() -> addWidget(std::make_unique<wt::WText>("<h4>User: </h4>"));
    editLoginName = root() -> addWidget(std::make_unique<wt::WLineEdit>());
    txtLoginPass = root() -> addWidget(std::make_unique<wt::WText>("<h4>Password: </h4>"));
    editLoginPass = root() -> addWidget(std::make_unique<wt::WLineEdit>());
    btnLoginSystem = root() -> addWidget(std::make_unique<wt::WPushButton>("Login to System"));
	Helpers::hide(txtLoginName, editLoginName, txtLoginPass, editLoginPass, btnLoginSystem);

    txtRegisterName = root() -> addWidget(std::make_unique<wt::WText>("<h4>Username: </h4>"));
    editRegisterName = root() -> addWidget(std::make_unique<wt::WLineEdit>());
    txtRegisterPass = root() -> addWidget(std::make_unique<wt::WText>("<h4>Password: </h4>"));
    editRegisterPass = root() -> addWidget(std::make_unique<wt::WLineEdit>());
    btnRegisterSystem = root() -> addWidget(std::make_unique<wt::WPushButton>("Register to System"));
	Helpers::hide(txtRegisterName, editRegisterName, txtRegisterPass, editRegisterPass, btnRegisterSystem);

	isHome();

	{
		std::string line;
		std::ifstream file(DATAFILE);

		if(file.is_open())
		{
			while(std::getline(file, line))
			{
				allPosts.push_back(root() -> addWidget(std::make_unique<wt::WText>(line)));
				allPosts.push_back(root() -> addWidget(std::make_unique<wt::WText>("<h1></h1>")));
			}	
			
			file.close();
		}
	}
}

void MainApplication::setupDatabase()
{
    std::unique_ptr<dbo::backend::Sqlite3> sqlite3{ new dbo::backend::Sqlite3(DATABASE) };
    sqlite3->setProperty("show-queries", "true");

    session.setConnection(std::move(sqlite3));
    session.mapClass<User>("Accounts");

    try
    {
        session.createTables();
    }
    catch(wt::Dbo::Exception& e){};
}

void MainApplication::isHome()
{
    Helpers::hide(txtLoginName, editLoginName, txtLoginPass, editLoginPass, btnLoginSystem);
    Helpers::hide(txtRegisterName, editRegisterName, txtRegisterPass, editRegisterPass, btnRegisterSystem);

    if(LoginTxtMsg != nullptr)
       	LoginTxtMsg -> hide();

    std::string str = txtLoggedName -> text().toUTF8();
    auto found = str.find("Visitor");
    
    if(found == std::string::npos)
        Helpers::show(txtCommitText, editCommitText, btnCommitText);

	found = str.find(ADMIN_USER);
   	if(found != std::string::npos)
   		Helpers::show(txtDeletePost, editDeletePost, btnDeletePost);

    btnCommitText -> clicked().connect(this, [this](){
        std::string str = txtLoggedName -> text().toUTF8();
        str = str.substr(14);
        str.erase(str.end() - 5, str.end());
        allPosts.push_back(root() -> addWidget(std::make_unique<wt::WText>("<h1></h1>")));
        allPosts.push_back(root() -> addWidget(std::make_unique<wt::WText>(editCommitText -> text() + " - by " + str)));

        std::ofstream outfile;
        outfile.open(DATAFILE, std::ios_base::app);
        outfile << editCommitText -> text().toUTF8() + " - by " + str + "\n";
        outfile.close();
    });

    btnDeletePost -> clicked().connect(this, [this](){
		std::vector<std::string> strings;
		
    	for(auto& p : allPosts)
    	{
    		std::string str = p -> text().toUTF8();
    		if(str != "<h1></h1>")
    		{
	    		if(editDeletePost -> text().toUTF8() == str)
	       			p -> hide();
	    		else
	    			strings.push_back(str);
    		}
    	}

		std::ofstream outfile;
		outfile.open(DATAFILE);
		for(const auto& s : strings)
			outfile << s << "\n";
		outfile.close();
    });

    for(auto& ref : allPosts)
        ref -> show();
}

void MainApplication::isLogin()
{
    Helpers::show(txtLoginName, editLoginName, txtLoginPass, editLoginPass, btnLoginSystem);
    Helpers::hide(txtRegisterName, editRegisterName, txtRegisterPass, editRegisterPass, btnRegisterSystem);
    Helpers::hide(txtCommitText, editCommitText, btnCommitText, txtDeletePost, editDeletePost, btnDeletePost);
    
    if(LoginTxtMsg != nullptr)
    	LoginTxtMsg -> hide();

    for(auto& ref : allPosts)
        ref -> hide();

    btnLoginSystem -> clicked().connect(this, [this](){
        sqlite3* db;
        char* sql;
        char* zErrMsg = 0;
        int rc;

        rc = sqlite3_open(DATABASE, &db);

        if(rc)
        {
            std::cerr << "Can't open Database! " << std::endl; 
            sqlite3_close(db);
            return;
        }

        sql = "SELECT * from Accounts";

        rc = sqlite3_exec(db, sql, CallbackSql, nullptr, &zErrMsg);
        if(rc != SQLITE_OK)
        {
            std::cerr << "SQL Error: " << zErrMsg << std::endl;
            sqlite3_free(zErrMsg);
        }

        sqlite3_close(db);

		if(LoginTxtMsg == nullptr)
			LoginTxtMsg = root() -> addWidget(std::make_unique<wt::WText>(""));

		bool found = false;
        for(const auto& it : DatabaseData)
        {
            auto val = DatabaseData[it.first];
            
            if(editLoginName -> text().toUTF8() == val.first)
            {
                if(editLoginPass -> text().toUTF8() == val.second)
				{
					txtLoggedName -> setText("<h3>Logged in: " + val.first + "</h3>");
					found = true;
				}            
            }
        }

        if(editLoginName -> text().toUTF8() == ADMIN_USER)
        {
        	if(editLoginPass -> text().toUTF8() == ADMIN_PASS)
        	{
				txtLoggedName -> setText("<h3>Logged in: " + std::string(ADMIN_USER) + " </h3>");
				found = true;
        	}
        }

        if(found == false)
        {
			LoginTxtMsg -> setText("Incorrect username or password");
        	LoginTxtMsg -> show();
        }

   		if(editLoginName -> text().toUTF8() == "")
   		{
   			LoginTxtMsg -> setText("Username is empty!");
   			LoginTxtMsg -> show();
   		}
   			
   		else if(editLoginPass -> text().toUTF8() == "")
   		{
   			LoginTxtMsg -> setText("Password is empty!");
   			LoginTxtMsg -> show();
   		}
    });
}

void MainApplication::isRegister()
{
    Helpers::hide(txtLoginName, editLoginName, txtLoginPass, editLoginPass, btnLoginSystem);
    Helpers::show(txtRegisterName, editRegisterName, txtRegisterPass, editRegisterPass, btnRegisterSystem);
    Helpers::hide(txtCommitText, editCommitText, btnCommitText, txtDeletePost, editDeletePost, btnDeletePost);

    if(LoginTxtMsg != nullptr)
    	LoginTxtMsg -> hide();

	if(RegisterTxtMsg == nullptr)
		RegisterTxtMsg = root() -> addWidget(std::make_unique<wt::WText>(""));

	if(RegisterTxtMsg != nullptr)
		RegisterTxtMsg -> hide();

    for(auto& ref : allPosts)
        ref -> hide();

    // Add User
    btnRegisterSystem -> clicked().connect(this, [this](){
		bool ok = true;
		
   		if(editRegisterName -> text().toUTF8() == "")
   		{
   			RegisterTxtMsg -> setText("Username is empty!");
   			ok = false;
   			RegisterTxtMsg -> show();
   		}
   		else if(editRegisterPass -> text().toUTF8() == "")
   		{
   			RegisterTxtMsg -> setText("Password is empty!");
   			ok = false;
   			RegisterTxtMsg -> show();
   		}
   		else
   			RegisterTxtMsg -> hide();

    	if(ok == true)
    	{
	        dbo::Transaction transaction{session};
	
	        std::unique_ptr<User> user{ new User() };
	        user -> name     = editRegisterName -> text().toUTF8();
	        user -> password = editRegisterPass -> text().toUTF8();
	
	        session.add(std::move(user));
    	}
    });
}

int main(int argc, char **argv)
{
    return Wt::WRun(argc, argv, [](const Wt::WEnvironment& env) {
      return std::make_unique<MainApplication>(env);
    });
}